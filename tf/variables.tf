variable "aws_region" {
  description = "AWS region"
  type        = string
  default     = "us-east-1"
}

variable "aws_profile" {
  description = "AWS profile"
  type        = string
  default     = "personal"
}

variable "tags" {
  type = map(string)
  default = {
    Project = "gebdominion.org"
  }
}

variable "domain" {
  description = "Domain"
  type        = string
  default     = "gebdominion.org"
}

variable "domain_aliases" {
  description = "Website domain aliases"
  type        = list(any)
  default     = ["www.gebdominion.org"]
}

variable "mx_records" {
  description = "MX DNS records"
  type        = string
  default     = "10 inbound-smtp.us-east-1.amazonaws.com"
}
