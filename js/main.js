// fullcalendar.io initialization
$(function() {
  if (window.location.hash) {
    var hash = window.location.hash.substring(1);

    if (hash == 'thanks') {
      $('#thanks').html(
        "<strong>Thank you for your message! We'll be in contact shortly.</strong>"
      );
    }
  }

  $('#calendar').fullCalendar({
    googleCalendarApiKey: 'AIzaSyBuqJj6gq5P6PW_1BkX6dzoQxmCnSWfVJo',
    events: { googleCalendarId: 'dominionregion@guidingeyes.net' },
    defaultView: 'listWeek',
    firstDay: 1,
    eventClick: function(event) {
      if (event.url) {
        window.open(event.url);
        return false;
      }
    },
    eventRender: function(event, element) {
      if (typeof event.location !== 'undefined') {
        element
          .find('.fc-list-item-title')
          .append('<br /><i class="icon-location"></i>' + event.location);
      }

      if (typeof event.description !== 'undefined') {
        element.find('.fc-list-item-title').append('<br /><em>' + event.description + '</em>');
      }
    }
  });
});

$('#subscribe-calendar').click(function() {
  window.open(
    'https://calendar.google.com/calendar/r?cid=rhothbgfiptfmq99ch33sgdsog@group.calendar.google.com',
    '_blank',
    'noopener'
  );
});
